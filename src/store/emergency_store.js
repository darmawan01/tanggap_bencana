import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    markers: []
  },
  getters: {
    getMarkers (state) {
      return state.markers;
    }
  },
  mutations: {
    setMarkers (state, marker) {
      state.markers.push(marker);
    },
    clearMakers (state) {
      state.markers = [];
    },
    fetchMarkers (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('users')
        .collection('emergency')
        .onSnapshot(doc => {
          state.markers = [];
          doc.docs.forEach(data => {
            
            let position = {
              lat: data.data().dblLatitude,
              lng: data.data().dblLongitude
            };

            let now = new Date();
            let res = Math.abs(now - data.data().deviceTimestamp) / 1000;
            let hours = Math.floor(res / 3600) % 24;
            let marker = null;

            // Check Hourse
            if (hours < 1) {
              // If hours < 1 = active animation in marker
              marker = new window.google.maps.Marker({
                position: position,
                title: data.data().strUserName,
                animation: window.google.maps.Animation.BOUNCE
              });
            } else {
              // else disable animation in marker
              marker = new window.google.maps.Marker({
                position: position,
                title: data.data().strUserName
              });
            }
            state.markers.push(marker);
          });
        });
    }
  },
  actions: {
    fetchMarkers: ({ commit }) => {
      commit('fetchMarkers');
    },
    addMarker: ({ commit }, marker) => {
      commit('setMarkers', marker);
    },
    clearMarkers: ({ commit }) => {
      commit('clearMakers');
    }
  }
};
