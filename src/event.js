import { firebase } from './commons/firebase_common';

export default [
  {
    name: 'APP_LOGIN_SUCCESS',
    callback: e => {
      console.log(e);
      let router = window.getApp.$router;
      router.push({ path: '/admin/dashboard' });
    }
  },
  {
    name: 'APP_LOGOUT',
    callback: e => {
      
      firebase
        .auth()
        .signOut()
        .then(() => {
          
          window.getApp.snackbar = {
            show: true,
            color: 'green',
            text: 'Logout successfully.'
          };
          localStorage.removeItem('token');
          window.getApp.$router.push({ path: '/login' });
        })
        .catch(error => {
          console.log(error);
          // An error happened.
        });
    }
  },
  {
    name: 'APP_PAGE_LOADED',
    callback: e => {}
  },
  {
    name: 'APP_AUTH_FAILED',
    callback: e => {
      window.getApp.$router.push('/login');
      window.getApp.$message.error('Token has expired');
    }
  },
  {
    name: 'APP_BAD_REQUEST',
    // @error api response data
    callback: msg => {
      window.getApp.$message.error(msg);
    }
  },
  {
    name: 'APP_ACCESS_DENIED',
    // @error api response data
    callback: msg => {
      window.getApp.$message.error(msg);
      window.getApp.$router.push('/forbidden');
    }
  },
  {
    name: 'APP_RESOURCE_DELETED',
    // @error api response data
    callback: msg => {
      window.getApp.$message.success(msg);
    }
  },
  {
    name: 'APP_RESOURCE_UPDATED',
    // @error api response data
    callback: msg => {
      window.getApp.$message.success(msg);
    }
  }
];
