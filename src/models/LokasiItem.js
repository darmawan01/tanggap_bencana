export default class LokasiItem {
  constructor () {
    this.id = null;
    this.dblLatitude = null;
    this.dblLongitude = null;
    this.dblAccuracy = null;
    this.strUserID = null;
    this.intSektor = null;
    this.strKodeKab = null;
    this.strKodeKec = null;
    this.strKodeDes = null;
    this.longTimestamp = null;
  }
}