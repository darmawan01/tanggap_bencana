import firebase from 'firebase';

let config = {
  apiKey: 'AIzaSyDPokYFh_7jD2fZXA4ZI4zHX7u47Fq5Kt8',
  authDomain: 'kogasgabpad.firebaseapp.com',
  databaseURL: 'https://kogasgabpad.firebaseio.com',
  projectId: 'kogasgabpad',
  storageBucket: 'kogasgabpad.appspot.com',
  messagingSenderId: '524684439402'
};
// let config = {
//   apiKey: 'AIzaSyC-tNQA7-0v_FOQa9li6ObAgcIi5lerDfs',
//   authDomain: 'tbdev-cdf40.firebaseapp.com',
//   databaseURL: 'https://tbdev-cdf40.firebaseio.com',
//   projectId: 'tbdev-cdf40',
//   storageBucket: 'tbdev-cdf40.appspot.com',
//   messagingSenderId: '751934446073'
// };


// Initial firebase with config
firebase.initializeApp(config);

let firestore = firebase.firestore();

firebase.auth().languageCode = 'id';


export { firebase, firestore };
