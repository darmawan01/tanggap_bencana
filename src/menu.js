const Menu = [
  { header: 'Apps' },
  {
    title: 'Dashboard',
    group: 'apps',
    icon: 'dashboard',
    name: 'Dashboard'
  },

  {
    title: 'Master Data',
    group: 'data',
    component: 'data',
    icon: 'widgets',
    items: [
      {
        name: 'posko_ungsi',
        title: 'Posko Pengungsian',
        component: 'data/posko_ungsi'
      },
      {
        name: 'posko_bantuan',
        title: 'Posko Bantuan',
        component: 'data/posko_bantuan'
      },
      {
        name: 'fasilitas_umum',
        title: 'Fasilitas Umum',
        badge: 'new',
        component: 'data/fasilitas_umum'
      },
      {
        name: 'fasilitas_ibadah',
        title: 'Fasilitas Ibadah',
        component: 'data/fasilitas_ibadah'
      },
      {
        name: 'fasilitas_pendidikan',
        title: 'Fasilitas Pendidikan',
        component: 'data/fasilitas_pendidikan'
      },
      {
        name: 'fasilitas_kesehatan',
        title: 'Fasilitas Kesehatan',
        component: 'data/fasilitas_kesehatan'
      },
      {
        name: 'infrastruktur',
        title: 'Infrastruktur',
        component: 'data/infrastruktur'
      },
      {
        name: 'rumah_rusak',
        title: 'Rumah Rusak',
        component: 'data/rumah_rusak'
      }
    ]
  },
  {
    title: 'Data Personal',
    group: 'person',
    component: 'person',
    icon: 'widgets',
    items: [
      {
        title: 'Data Relawan',
        name: 'relawan',
        component: 'person/relawan'
      },
    ]
  },
  {
    title: 'Emergency Calls',
    group: 'features',
    icon: 'map',
    name: 'features/emergency_calls'
  }
];
// reorder menu
Menu.forEach(item => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });
  }
});

export default Menu;
