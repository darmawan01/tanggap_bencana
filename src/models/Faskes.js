import Facility from './Facility';

export default class Faskes extends Facility {
  constructor (strNamaLembaga, strJenisLembaga, kerusakan, reruntuhan) {
    super(kerusakan, reruntuhan);
    this.strNamaLembaga = strNamaLembaga;
    this.strJenisLembaga = strJenisLembaga;
    this.masihBeroperasi = null;
    this.strTypeFasilitas = null;
  }
}