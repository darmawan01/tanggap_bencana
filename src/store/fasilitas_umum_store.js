import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    umums: []
  },
  getters: {
    getUmums (state) {
      return state.umums;
    }
  },
  mutations: {
    setUmums (state, umum) {
      state.umums.push(umum);
    },
    addUmum (state, umum) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('fasum')
        .collection('list')
        .add(umum).then(res => {
          console.log(res);
        }).catch(error => {
          console.log(error);
        });
    },
    clearUmums (state) {
      state.umums = [];
    },
    fetchUmums (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('fasum')
        .collection('list')
        .onSnapshot(doc => {
          state.umums = [];
          for (let i = 0; i < doc.docs.length; i++) {
            state.umums.push(doc.docs[i].data());
          }
        });
    }
  },
  actions: {
    fetchUmums: ({ commit }) => {
      commit('fetchUmums');
    },
    addUmum: ({ commit }, umum) => {
      commit('addUmum', umum);
    },
    setUmums: ({ commit }, umum) => {
      commit('setUmums', umum);
    },
    clearUmums: ({ commit }) => {
      commit('clearUmums');
    }
  }
};
